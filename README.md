# 交通信息上报钉钉小程序

#### 介绍

交通信息上报钉钉小程序

#### 软件架构

- SpringBoot
- MybatisPlus
- MySQL
- Redis
- Ant Design Mini
- 七牛云：图片视频存储

#### 详情

![输入图片说明](dd_apps/pages/images/001.png)
![输入图片说明](dd_apps/pages/images/2001.jpg)
![输入图片说明](dd_apps/pages/images/2002.jpg)
![输入图片说明](dd_apps/pages/images/2004.jpg)
