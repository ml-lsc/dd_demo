App({
  onLaunch(options) {
    // 第一次打开
    console.info('App onLaunch');
    if (this.authCode=='') {
      dd.getAuthCode({
        success: function (res) {
          console.log(res);
          this.authCode=res.data
          dd.setStorage({
            key: 'authCode',
            data: {
              authCode:res
            },
            success: function() {
              console.log('写入成功',res);
            }
          });
        }
      }); 
      dd.alert({
        title: '授权',
        content: '获取用户名和用户id',
        buttonText: '确认',
        success: () => { 
          this.toLogin();
        },
      });
    }
  },
  toLogin() {
    let that=this
    dd.getStorage({
      key: 'authCode',
      success: function(res) {
        console.log( '获取成功：' + res.data.authCode.authCode);
        const authCode=res.data.authCode.authCode
        dd.httpRequest({
          url: "http://127.0.0.1:8089/login",
          method: 'post', 
          data:{
            authCode:authCode
          },
          dataType: 'json',
          success: function (res) {
            console.log(res.data);
            console.log("用户id",res.userId);
            // that.userId=res.userId;
          },
          fail: function (res) {
          },
          complete: function (res) {
          }
        });
      },
    });
  }, 
  userId:'29690357681256414',
  username:'林邵晨',
  authCode:''
}); 
 