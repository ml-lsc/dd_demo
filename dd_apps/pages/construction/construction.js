let app = getApp()
let domain = 'http://localhost:8089'
let getByTypeUrl = domain + '/get/' + app.userId
let getByIdUrl = domain + '/get/one/'
Page({
  data: {
    position: 'bottom', 
    animation: true,
    scrollVisible: true,
    closeVisile: false,
    current: 0,
    content:[],
    items: [
      {
        title:"",
      },{
        title:"",
      },{
        title:"",
      },
      {
        title:"",
      },
    ],
  },
  onLoad(){
    this.getData(4);
    this.getData(3);
    this.getData(2);
    this.getData(1);
  },
  // 查询列表
  getData(cur){
    let that=this
    dd.httpRequest({
      headers: {
        "Content-Type": "application/json"
      },
      url: getByTypeUrl + '/' + cur,
      method: 'GET',
      dataType: 'json',
      success: function(res) {
        if (cur-1==0) {
          that.setData({
            content:res.data,
            'items[0].title':'进行中('+res.data.length+')'
          }) 
          return;
        }
        if (cur-1==1) {
          that.setData({
            content:res.data,
            'items[1].title':'未开始('+res.data.length+')'
          }) 
          return;
        }
        if (cur-1==2) {
          that.setData({
            content:res.data,
            'items[2].title':'已暂停('+res.data.length+')'
          }) 
          return;
        }
        if (cur-1==3) {
          that.setData({
            content:res.data,
            'items[3].title':'已结束('+res.data.length+')'
          }) 
        }
        
      },
    });
  },
  onChange(current) {
    this.setData({
      current,
    });
    this.getData(current+1);
  },
  toForm(){
    dd.navigateTo({
      url: '../form/form'
    })
    
  },
 
  //打开详情页
  handleShowBasic(e) {
    dd.navigateTo({
      url: '../detail/detail?id='+e.target.dataset.id
    })
    
  },
 
});
