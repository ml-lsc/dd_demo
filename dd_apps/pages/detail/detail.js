let domain = 'http://localhost:8089'
let getByIdUrl = domain + '/get/one/'
Page({
  data: {
    form:{}
  },
  onLoad(options) {
    dd.showLoading({
      content: '加载中...',
    });
    
    let  id=options.id 
    //查询详情 
    let that=this
    // 设置加载时间
    setTimeout(() => {
      dd.hideLoading();
      dd.httpRequest({
        headers: {
          "Content-Type": "application/json"
        },
        url: getByIdUrl + id,
        method: 'GET',
        dataType: 'json',
        success: function(res) {   
          that.setData({
            form:res.data
          })
        },
      });
    }, 1500)
    
  },
  goBack(){
    dd.navigateTo({
      url: '../construction/construction'
    })
  },
  previewImage(e){
    let urls=[e.target.dataset.url];
    dd.previewImage({
      current: 0,
      urls: urls
    });
  }
});
