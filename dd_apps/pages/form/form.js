//page/component/form/form.js
let app = getApp()
let domain = 'http://127.0.0.1:8089'
let url = domain + '/form'
let uploadUrl = domain + '/upload'
let initDataUrl = domain + '/data/dictionary/list' 
let arr = [1,2,3,4,5,6,7,8,9]

Page({
  data: {
    route: [],
    indexRoute: -1,
    road: [],
    indexRoad: -1,
    roadConditionType: [],
    indexRoadType: -1,
    cause: [],
    indexCause: -1,
    roadCondition: [],
    indexRoadCondition: -1,
    administrative: [],
    indexAdministrative: -1,
    province: [],
    indexProvince: -1,
    startTime: '',
    restoreTime: '',
    template: [],
    indexTemplate: -1,
    desc: [],
    position: 'bottom',
    animation: true,
    scrollVisible: false,
    closeVisile: false,
    // 表单数据
    form: {
      routeName: '',
      roadName: '',
      startNumber: '',
      endNumber: '', 
      place: '',
      roadDirection: '',
      roadType: '',
      cause: '',
      startTime: '',
      restoreTime: '',
      roadCondition: '',
      template: '',
      constructionDescription: '',
      administrative: '',
      province: '',
      plan: '',
      imgList: [],
      video: '',
      type: 2, 
      uid: ''
    },
    videoUrl:''
  },
 
  onReady() {
    this.formReset();
    this.getRouteData();
    this.getRoadData();
    this.getRoadConditionTypeData();
    this.getCauseData();
    this.getRoadConditionData();
    this.getAdministrativeData();
    this.getProvinceData();
    this.getTemplateData();
    this.getDescData();
    this.data.form.uid = app.userId;
    this.data.form.type = 2;
    this.mapCtx = dd.createMapContext('map');
    if (dd.canIUse('createMapContext')) {
      this.mapCtx.moveToLocation();
    }
  },
  formSubmit: function () {
    let that=this
    console.log(this.data.form);
    let formData=this.data.form;
    let imgList=''
    if (formData.imgList.length>0) {
      formData.imgList.forEach((e,index)=>{
        if (index!=0) {
          imgList+=','
        }
        imgList+=e;
      })
    }
    if (formData.routeName.length==0) {
      dd.showToast({
        type: 'fail',
        content: '请选择路线名称',
        duration: 1500
      });
      return;
    }
    if (formData.roadName.length==0) {
      dd.showToast({
        type: 'fail',
        content: '请选择路段名称',
        duration: 1500
      });
      return;
    }
    if (formData.startNumber==0) {
      dd.showToast({
        type: 'fail',
        content: '请输入起始桩号',
        duration: 1500
      });
      return;
    }
    if (formData.endNumber==0) {
      dd.showToast({
        type: 'fail',
        content: '请输入截止桩号',
        duration: 1500
      });
      return;
    }
    if (formData.place.length==0) {
      dd.showToast({
        type: 'fail',
        content: '请输入施工位置',
        duration: 1500
      });
      return;
    }
    if (formData.roadDirection.length==0) {
      dd.showToast({
        type: 'fail',
        content: '请输入路段方向',
        duration: 1500
      });
      return;
    }
    if (formData.roadType.length==0) {
      dd.showToast({
        type: 'fail',
        content: '请选择路况类型',
        duration: 1500
      });
      return;
    }
    if (formData.cause.length==0) {
      dd.showToast({
        type: 'fail',
        content: '请选择施工原因',
        duration: 1500
      });
      return;
    }
    if (formData.startTime.length==0) {
      dd.showToast({
        type: 'fail',
        content: '请选择开始时间',
        duration: 1500
      });
      return;
    }
    if (formData.restoreTime.length==0) {
      dd.showToast({
        type: 'fail',
        content: '请选择恢复时间',
        duration: 1500
      });
      return;
    }
    if (formData.roadCondition.length==0) {
      dd.showToast({
        type: 'fail',
        content: '请选择道路状态',
        duration: 1500
      });
      return;
    }
    if (formData.template.length==0) {
      dd.showToast({
        type: 'fail',
        content: '请选择描述模板',
        duration: 1500
      });
      return;
    }
    if (formData.administrative.length==0) {
      dd.showToast({
        type: 'fail',
        content: '请选择行政区域',
        duration: 1500
      });
      return;
    }
    if (formData.province.length==0) {
      dd.showToast({
        type: 'fail',
        content: '请选择影响邻省',
        duration: 1500
      });
      return;
    }
    if (formData.plan.length==0) {
      dd.showToast({
        type: 'fail',
        content: '请输入通行方案',
        duration: 1500
      });
      return;
    }
    dd.httpRequest({
      headers: {
        "Content-Type": "application/json"
      },
      url: url,
      method: 'POST',
      data: JSON.stringify({
        routeName: formData.routeName,
        roadName: formData.roadName,
        startNumber: formData.startNumber,
        endNumber: formData.endNumber,
        place: formData.place,
        roadDirection: formData.roadDirection,
        roadType: formData.roadType,
        cause: formData.cause,
        startTime: formData.startTime,
        restoreTime: formData.restoreTime,
        roadCondition: formData.roadCondition,
        template: formData.template,
        constructionDescription: formData.constructionDescription,
        administrative: formData.administrative,
        province: formData.province,
        plan: formData.plan,
        imgList: imgList,
        video: formData.video,
        type: formData.type,
        uid: formData.uid
      }),
      dataType: 'text',
      success: function(res) {
        console.log(res);
        dd.showToast({
          type: 'success',
          content: '上报成功',
          duration: 3000
        });
        that.formReset();
        dd.navigateTo({
          url: '../construction/construction'
        })
      },
      fail: function(res) {
        dd.showToast({
          type: 'fail',
          content: '上报失败',
          duration: 3000
        });
      },
    });
    
  },
  formReset: function () {
    this.setData({ 
      indexRoute: -1,
      indexRoad: -1,
      indexRoadType: -1,
      indexCause: -1,
      indexRoadCondition: -1,
      indexAdministrative: -1,
      indexProvince: -1,
      indexTemplate: -1,
      videoUrl:'',
      startTime: '',
      restoreTime: '',
      form:{
        routeName: '',
        roadName: '',
        startNumber: '',
        endNumber: '', 
        place: '',
        roadDirection: '',
        roadType: '',
        cause: '',
        startTime: '',
        restoreTime: '',
        roadCondition: '',
        template: '',
        constructionDescription: '',
        administrative: '',
        province: '',
        plan: '',
        imgList: [],
        video: '',
      }
    });
  },
  reset(){
    dd.showLoading({
      content: '重置中...',
    });
    // 设置加载时间
    setTimeout(() => {
         dd.hideLoading();
    }, 500)
    dd.navigateTo({
      url: '../construction/construction'
    })
    dd.navigateTo({
      url: '../form/form'
    })
  },
  // 路线名称
  changeRoute(e) {
    this.setData({
      indexRoute: e.detail.value,
    });
    this.data.form.routeName = this.data.route[e.detail.value]
  },
  // 路段名称
  changeRoad(e) {
    this.setData({
      indexRoad: e.detail.value,
    });
    this.data.form.roadName = this.data.road[e.detail.value]
  },
  startNumber(e) {
    this.data.form.startNumber = e.detail.value
  },
  endNumber(e) {
    this.data.form.endNumber = e.detail.value
  },
  place(e) {
    this.data.form.place = e.detail.value
  },
  roadDirection(e) {
    this.data.form.roadDirection = e.detail.value
  },
  // 路况类型
  changeRoadType(e) {
    this.setData({
      indexRoadType: e.detail.value,
    });
    this.data.form.roadType = this.data.roadConditionType[e.detail.value]
  },
  // 施工原因
  changeCause(e) {
    this.setData({
      indexCause: e.detail.value,
    });
    this.data.form.cause = this.data.cause[e.detail.value]
  },
  // 开始时间
  startTime() {
    const time = new Date().toLocaleString().replaceAll("/", "-");
    dd.datePicker({
      format: 'yyyy-MM-dd HH:mm',
      currentDate: time,
      success: (res) => {
        this.setData({
          startTime: res.date
        })
        this.data.form.startTime = res.date
      },
    });
  },
  // 恢复时间
  restoreTime() {
    const time = new Date().toLocaleString().replaceAll("/", "-");
    dd.datePicker({
      format: 'yyyy-MM-dd HH:mm',
      currentDate: time,
      success: (res) => {
        this.setData({
          restoreTime: res.date
        })
        this.data.form.restoreTime = res.date
      },
    });
  },
  // 道路状态
  changeRoadCondition(e) {
    this.setData({
      indexRoadCondition: e.detail.value,
    });
    this.data.form.roadCondition = this.data.roadCondition[e.detail.value]
  },
  // 施工模板
  changeTemplate(e) {
    let desc=this.data.desc[e.detail.value]
    this.setData({
      indexTemplate: e.detail.value,
      'form.constructionDescription':desc
    });
    this.data.form.template = this.data.template[e.detail.value]
  },
  // 施工描述
  bindTextAreaBlur(e) {
    this.data.form.constructionDescription = e.detail.value
  },
  // 行政区域
  changeAdministrativen(e) {
    this.setData({
      indexAdministrative: e.detail.value,
    });
    this.data.form.administrative = this.data.administrative[e.detail.value]
  },
  // 影响邻省
  changeProvince(e) {
    this.setData({
      indexProvince: e.detail.value,
    });
    this.data.form.province = this.data.province[e.detail.value]
  },
  // 通行方案
  trafficPlanText(e) {
    this.data.form.plan = e.detail.value;
  },
  //打开地图
  handleShowBasic() {
    this.setData({
      closeVisile: true,
    });
  },
  //关闭地图
  handlePopupClose() {
    this.setData({
      closeVisile: false
    });
  },
  onChange(fileList) {
    console.log('图片列表：', fileList);
  },
  // 上传图片
  onUpload(localFile) {
    return new Promise((resolve, reject) => {
      my.uploadFile({
        url: uploadUrl, 
        fileType: 'image',
        name: 'file', 
        filePath: localFile.path, 
        formData: { 
          file: localFile.path 
        }, 
        "Content-Type": "multipart/form-data",
        'accept': 'application/json',
        success: res => {
          this.data.form.imgList.push(res.data);
          resolve(res.data);
        },
        fail: err => {
          reject();
        },
      });
    });
  },
  onRemove(file) {
    return new Promise((resolve) => {
      console.log('即将移除的图片为：', file);
      my.confirm({
        title: '是否确认移除图片',
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        success: (e) => {
          this.data.form.imgList.splice(this.data.form.imgList.indexOf(file.url),1);
          resolve(e.confirm);
        }
      });
    })
  },
  // 上传视频
  openVideo(){
    var that=this
    dd.chooseVideo({
      sourceType: ['album','camera'],
      maxDuration: 60,
      success:(res)=> {
        dd.showLoading({
          content: '上传中...',
        });
        // 设置加载时间
        setTimeout(() => {
             dd.hideLoading();
        }, 5000)
        my.uploadFile({
          url: uploadUrl, 
          fileType: 'video',
          name: 'file', 
          filePath: res.filePath,
          formData: { 
            file: res.filePath
           }, 
          "Content-Type": "multipart/form-data",
          'accept': 'application/json',
          success: res => {
            that.setData({
              videoUrl:res.data
            })
            that.data.form.video=res.data;
          },
        });
      },
      fail: (err)=> {
        console.log(err)
      }
    })
  },
  deleteVideo(){
    this.setData({
      videoUrl:'',
      form:{
        video:''
      }
    })
  },
  getRouteData(){
    let that=this
    dd.httpRequest({ 
      headers: {
        "Content-Type": "application/json" 
      },
      url: initDataUrl + '/' + arr[0],
      method: 'GET',
      dataType: 'json',
      success: function(res) {
        that.setData({
          route:res.data
        }) 
      },
    });
  },
  getRoadData(){
    let that=this
    dd.httpRequest({ 
      headers: {
        "Content-Type": "application/json" 
      },
      url: initDataUrl + '/' + arr[1],
      method: 'GET',
      dataType: 'json',
      success: function(res) {
        that.setData({
          road:res.data
        }) 
      },
    });
  },
  getRoadConditionTypeData(){
    let that=this
    dd.httpRequest({ 
      headers: {
        "Content-Type": "application/json" 
      },
      url: initDataUrl + '/' + arr[2],
      method: 'GET',
      dataType: 'json',
      success: function(res) {
        that.setData({
          roadConditionType:res.data
        }) 
      },
    });
  },
  getCauseData(){
    let that=this
    dd.httpRequest({ 
      headers: {
        "Content-Type": "application/json" 
      },
      url: initDataUrl + '/' + arr[3],
      method: 'GET',
      dataType: 'json',
      success: function(res) {
        that.setData({
          cause:res.data
        }) 
      },
    });
  },
  getRoadConditionData(){
    let that=this
    dd.httpRequest({ 
      headers: {
        "Content-Type": "application/json" 
      },
      url: initDataUrl + '/' + arr[4],
      method: 'GET',
      dataType: 'json',
      success: function(res) {
        that.setData({
          roadCondition:res.data
        }) 
      },
    });
  },
  getAdministrativeData(){
    let that=this
    dd.httpRequest({ 
      headers: {
        "Content-Type": "application/json" 
      },
      url: initDataUrl + '/' + arr[5],
      method: 'GET',
      dataType: 'json',
      success: function(res) {
        that.setData({
          administrative:res.data
        }) 
      },
    });
  },
  getProvinceData(){
    let that=this
    dd.httpRequest({ 
      headers: {
        "Content-Type": "application/json" 
      },
      url: initDataUrl + '/' + arr[6],
      method: 'GET',
      dataType: 'json',
      success: function(res) {
        that.setData({
          province:res.data
        }) 
      },
    });
  },
  getTemplateData(){
    let that=this
    dd.httpRequest({ 
      headers: {
        "Content-Type": "application/json" 
      },
      url: initDataUrl + '/' + arr[7],
      method: 'GET',
      dataType: 'json',
      success: function(res) {
        that.setData({
          template:res.data
        }) 
      },
    });
  },
  getDescData(){
    let that=this
    dd.httpRequest({ 
      headers: {
        "Content-Type": "application/json" 
      },
      url: initDataUrl + '/' + arr[8],
      method: 'GET',
      dataType: 'json',
      success: function(res) {
        that.setData({
          desc:res.data
        }) 
      },
    });
  },
})