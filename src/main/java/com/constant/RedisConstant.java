package com.constant;

public class RedisConstant {

    /**
     * 数据字典缓存key
     */
    public static final String DATA_DICTIONARY = "data:dictionary:";

    /**
     * 存在时间
     */
    public static final int DATA_DICTIONARY_TIME = 30;

    /**
     * 锁key
     */
    public static final String MUTEX = "mutex:";

    /**
     * 存在时间
     */
    public static final int MUTEX_TIME = 10;

    /**
     * 计划详情缓存key
     */
    public static final String CON_INFO = "con:info:";

    /**
     * 存在时间
     */
    public static final int CON_INFO_TIME = 30;

    /**
     * 空值存在时间
     */
    public static final int NULL_TIME = 1;

}
