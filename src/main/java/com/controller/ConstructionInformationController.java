package com.controller;

import com.dto.ConDTO;
import com.dto.ConstructionInformationDTO;
import com.entity.ConstructionInformation;
import com.service.ConstructionInformationService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 施工信息表 前端控制器
 * </p>
 *
 * @author 马磊
 * @since 2023-02-10
 */
@RestController
public class ConstructionInformationController {

    @Resource
    private ConstructionInformationService constructionInformationService;

    @ApiOperation("上传图片")
    @PostMapping(path = "/upload",produces = "application/json")
    public String upload(MultipartFile file){
        return constructionInformationService.upload(file);
    }

    @ApiOperation("上报信息")
    @PostMapping(path = "/form",produces = "application/json")
    public String add(@RequestBody ConstructionInformation con) throws IllegalAccessException {
        return constructionInformationService.add(con);
    }

    @ApiOperation("查询信息")
    @GetMapping("/get/{userId}/{type}")
    public List<ConDTO> getInfo(@PathVariable String userId,@PathVariable Integer type){
        return constructionInformationService.getInfo(userId,type);
    }

    @ApiOperation("根据id查询详情信息")
    @GetMapping("/get/one/{id}")
    public ConstructionInformationDTO selectById(@PathVariable Integer id){
        return constructionInformationService.selectById(id);
    }


}
