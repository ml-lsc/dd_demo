package com.controller;


import com.dto.DataDictionaryDTO;
import com.entity.DataDictionary;
import com.service.DataDictionaryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 数据字典表 前端控制器
 * </p>
 *
 * @author 马磊
 * @since 2023-02-16
 */
@RestController
@RequestMapping("/data/dictionary")
public class DataDictionaryController {

    @Resource
    private DataDictionaryService dataDictionaryService;

    @ApiOperation("初始化数据")
    @GetMapping("/list/{type}")
    public List<DataDictionaryDTO> getInitData(@PathVariable Integer type){
        return dataDictionaryService.getInitData(type);
    }
}

