package com.controller;


import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.entity.User;
import com.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/**
 * <p>
 * 上报人表 前端控制器
 * </p>
 *
 * @author 马磊
 * @since 2023-02-10
 */
@RestController
public class UserController {

    @Resource
    private UserService userService;

    /**
     * @param authCode
     */
    @ApiOperation("用户登录")
    @PostMapping(value = "/login")
    public User login(String authCode) {
        return userService.login(authCode);
    }

}

