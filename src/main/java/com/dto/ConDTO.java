package com.dto;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class ConDTO {

    private Integer id;

    private Integer type;

    private String title;

    private String content;

}
