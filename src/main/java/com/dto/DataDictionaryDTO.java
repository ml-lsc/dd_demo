package com.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DataDictionaryDTO {

    @ApiModelProperty(value = "主键id")
    private Integer id;


    @ApiModelProperty(value = "数据内容")
    private String value;
}
