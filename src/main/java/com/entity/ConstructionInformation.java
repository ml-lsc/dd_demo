package com.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 施工信息表
 * </p>
 *
 * @author 马磊
 * @since 2023-02-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="TbConstructionInformation对象", description="施工信息表")
@TableName(value = "tb_construction_information",autoResultMap = true)
public class ConstructionInformation extends Model<ConstructionInformation> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "路线名称")
    private String routeName;

    @ApiModelProperty(value = "路段名称")
    private String roadName;

    @ApiModelProperty(value = "起始桩号")
    private Integer startNumber;

    @ApiModelProperty(value = "截止桩号")
    private Integer endNumber;

    @ApiModelProperty(value = "施工位置")
    private String place;

    @ApiModelProperty(value = "路段方向")
    private String roadDirection;

    @ApiModelProperty(value = "路况类型")
    private String roadType;

    @ApiModelProperty(value = "施工原因")
    private String cause;

    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "恢复时间")
    private LocalDateTime restoreTime;

    @ApiModelProperty(value = "道路状态")
    private String roadCondition;

    @ApiModelProperty(value = "描述模板")
    private String template;

    @ApiModelProperty(value = "施工描述")
    private String constructionDescription;

    @ApiModelProperty(value = "行政区域")
    private String administrative;

    @ApiModelProperty(value = "影响邻省")
    private String province;

    @ApiModelProperty(value = "通行方案")
    private String plan;

    @ApiModelProperty(value = "现场图片")
    private String imgList;

    @ApiModelProperty(value = "现场视频")
    private String video;

    @ApiModelProperty(value = "事件状态：1 未开始，2 进行中，3 已暂停，4 已结束")
    private Integer type;

    @ApiModelProperty(value = "用户id")
    private String uid;

    @ApiModelProperty(value = "逻辑删除：0 未删除，1删除")
    private Integer deleted;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
