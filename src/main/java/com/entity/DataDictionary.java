package com.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 数据字典表
 * </p>
 *
 * @author 马磊
 * @since 2023-02-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="TbDataDictionary对象", description="数据字典表")
@TableName(value = "tb_data_dictionary",autoResultMap = true)
public class DataDictionary extends Model<DataDictionary> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "数据类型：1 route,2 road,3 type,4 cause,5 condition,6 admin,7 province,8 template,9 desc")
    private Integer type;

    @ApiModelProperty(value = "数据内容")
    private String value;

    @ApiModelProperty(value = "逻辑删除：0 未删除，1 删除")
    private Integer deleted;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
