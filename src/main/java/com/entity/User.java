package com.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * <p>
 * 上报人表
 * </p>
 *
 * @author 马磊
 * @since 2023-02-10
 */
@Data
@AllArgsConstructor
public class User extends Model<User> {


    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "用户id")
    private String userId;


    public User() {

    }
}
