package com.mapper;

import com.entity.ConstructionInformation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 施工信息表 Mapper 接口
 * </p>
 *
 * @author 马磊
 * @since 2023-02-10
 */
@Mapper
public interface ConstructionInformationMapper extends BaseMapper<ConstructionInformation> {

}
