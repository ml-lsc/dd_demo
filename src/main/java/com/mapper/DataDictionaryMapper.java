package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dto.DataDictionaryDTO;
import com.entity.DataDictionary;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 数据字典表 Mapper 接口
 * </p>
 *
 * @author 马磊
 * @since 2023-02-16
 */
@Mapper
public interface DataDictionaryMapper extends BaseMapper<DataDictionary> {


    @Select("select id,value from tb_data_dictionary where type = #{type}")
    List<DataDictionaryDTO> selectByType(Integer type);
}
