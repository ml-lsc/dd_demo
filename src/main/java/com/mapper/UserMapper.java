package com.mapper;

import com.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 上报人表 Mapper 接口
 * </p>
 *
 * @author 马磊
 * @since 2023-02-10
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
