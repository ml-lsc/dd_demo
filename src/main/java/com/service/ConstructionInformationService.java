package com.service;

import com.dto.ConDTO;
import com.dto.ConstructionInformationDTO;
import com.entity.ConstructionInformation;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 施工信息表 服务类
 * </p>
 *
 * @author 马磊
 * @since 2023-02-10
 */
public interface ConstructionInformationService extends IService<ConstructionInformation> {

    String upload(MultipartFile file);

    String add(ConstructionInformation con) throws IllegalAccessException;

    List<ConDTO> getInfo(String userId, Integer type);

    ConstructionInformationDTO selectById(Integer id);
}
