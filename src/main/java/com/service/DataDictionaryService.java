package com.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dto.DataDictionaryDTO;
import com.entity.DataDictionary;

import java.util.List;

/**
 * <p>
 * 数据字典表 服务类
 * </p>
 *
 * @author 马磊
 * @since 2023-02-16
 */
public interface DataDictionaryService extends IService<DataDictionary> {

    List<DataDictionaryDTO> getInitData(Integer type);
}
