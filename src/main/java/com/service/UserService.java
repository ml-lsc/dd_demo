package com.service;

import com.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 上报人表 服务类
 * </p>
 *
 * @author 马磊
 * @since 2023-02-10
 */
public interface UserService extends IService<User> {

    User login(String authCode);
}
