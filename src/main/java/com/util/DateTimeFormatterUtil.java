package com.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 时间格式化
 */
public class DateTimeFormatterUtil {

    public static String formatter(LocalDateTime time){
        DateTimeFormatter dfDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return dfDateTime.format(time);
    }
}
