package com;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CodeGenerationTestDTO {

    /**
     * 代码生成
     */
    @Test
    void run(){

        GlobalConfig config = new GlobalConfig();
        config.setActiveRecord(true)
                .setAuthor("马磊")
                .setOutputDir("C:\\Users\\86178\\Desktop\\实习文件\\项目\\dd_demo"+"/src/main/java")
                .setFileOverride(true)
                .setIdType(IdType.AUTO)
                .setServiceName("%sService")
                .setSwagger2(true);

        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL)
                .setDriverName("com.mysql.cj.jdbc.Driver")
                .setUrl("jdbc:mysql://localhost:3306/demo?useUnicode=true&characterEncoding=UTF-8")
                .setUsername("root")
                .setPassword("root");

        StrategyConfig strategy = new StrategyConfig();
        strategy.setCapitalMode(true)
                .setNaming(NamingStrategy.underline_to_camel)
                .setColumnNaming(NamingStrategy.underline_to_camel)
                //.setTablePrefix("tb")
                .setInclude("tb_data_dictionary")
                .setEntityLombokModel(true)
                //.setLogicDeleteFieldName("deleted")
                //.setTableFillList(tableFills)
                //.setVersionFieldName("version")
                .setRestControllerStyle(true)
                .setControllerMappingHyphenStyle(true);

        PackageConfig packageConfig = new PackageConfig();
        packageConfig
                .setModuleName("com")
                .setParent("")
                .setMapper("mapper")
                .setService("service")
                .setController("controller")
                .setEntity("entity");
        //.setXml("mapper");
        AutoGenerator autoGenerator = new AutoGenerator();
        autoGenerator.setGlobalConfig(config)
                .setDataSource(dataSourceConfig)
                .setStrategy(strategy)
                .setPackageInfo(packageConfig);
        autoGenerator.execute();
    }

}
